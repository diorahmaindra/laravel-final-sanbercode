@extends('layouts/master')

@section('judul')
    <i class="fa fa-file-alt mr-2"></i><b>Edit Selected Post</b>
@endsection

@section('content')
<a href="/profile" class="btn btn-info btn-sm"><i class="fa fa-arrow-circle-left mr-1"></i> Back to My Profile </a>
<form action="/profile" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        <div class="form-group">
            <label for="post">New Post</label>
            <textarea name="post" id="" class="form-control" placeholder="What's happening?"></textarea>
            @error('post')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label for="image">Insert Image</label><br>
            <input type="file" name="image">
            @error('image')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <button type="submit" class="btn btn-primary">Update Post</button>
    </div>
    </form>   
@endsection