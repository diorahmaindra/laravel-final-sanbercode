<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route Template 
Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function () {
    return view('layouts.master');
});

Route::get('/timeline', function () {
    return view('timeline_menu.index');
});

Route::get('/profile', function () {
    return view('profil_menu.index');
});

Route::get('/profile/edit', function () {
    return view('profil_menu.edit');
});

Route::get('/post/create', function () {
    return view('post_menu.create');
});

Route::get('/post/show', function () {
    return view('post_menu.show');
});

Route::get('/post/edit', function () {
    return view('post_menu.edit');
});

